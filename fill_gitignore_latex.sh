#!/bin/bash

# Add targets to .gitignore
for t in `head -n 1 Makefile | perl -e 's/^.*=//' -p`
do 
    grep -q "$t.pdf" .gitignore || echo "$t.pdf" >> .gitignore
done

# Add to the Makefile the rules for fig->eps
# Add targets to .gitignore
for f in *.eps
do 
    base=`basename $f .eps`
    grep -q "$f" Makefile || echo "$base.eps: $base.fig" >> Makefile && echo -e "\tfig2dev -L eps $base.fig $base.eps" >> Makefile \
        test -f "$base".fig && echo "$f" >> .gitignore
done 

