#!/usr/bin/python3

import fileinput
import re
import argparse
import os

parser = argparse.ArgumentParser(description='Process Mutt aliases.')
parser.add_argument('--tags', dest='tags', action='store_true',
                    help='Output script to update notmuch tags')
parser.add_argument('--all', dest='all', action='store_true',
                    help='Output script to initialize notmuch tags')
parser.add_argument('--file', dest='filename', action='store',
                    help='Mutt aliases file to elaborate')

args = parser.parse_args()


for line in fileinput.input(os.path.expanduser(args.filename)):
    m = re.search('^\s*alias\s+(\S+)\s(.*)\s<?(\S*@\S*?)[\s>]', line)
    if m is not None:
        if args.tags:
            print('notmuch tag +' + m.group(1) + " tag:new and \(to:" + m.group(3) + " or from:" + m.group(3) + "\)")
        elif args.all:
            print('notmuch tag +' + m.group(1) + " to:" + m.group(3) + " or from:" + m.group(3) + "")
        else:
            print('virtual-mailboxes "' + m.group(2) + ' (1M)" "notmuch://?query=tag:' + m.group(1) + ' date:1M.."')
            print('virtual-mailboxes "' + m.group(2) + '" "notmuch://?query=tag:' + m.group(1) + '"')
