#! /bin/bash

# input resolution, currently fullscreen.
# you can set it manually in the format "WIDTHxHEIGHT" instead.
INRES=$(xrandr -q |  awk '/*/ {print $1}'i)
INRES=1366x768

# output resolution.
# keep the aspect ratio the same or your stream will not fill the display.
OUTRES="1280x720"

# input audio. You can use "/dev/dsp" for your primary audio input.
INAUD="pulse"

# target fps
FPS="25"

# video preset quality level.
# more FFMPEG presets avaiable in /usr/share/ffmpeg
QUAL="fast"

# stream key. You can set this manually, or reference it from a hidden file like what is done here.
STREAM_KEY=$1

# stream url. Note the formats for twitch.tv and justin.tv
# twitch:"rtmp://live.twitch.tv/app/$STREAM_KEY"
# justin:"rtmp://live.justin.tv/app/$STREAM_KEY"
STREAM_URL="rtmp://a.rtmp.youtube.com/live2/${STREAM_KEY}"

ffmpeg -f alsa -ac 2 -i hw:0,0 -f x11grab -framerate "$FPS" -video_size "$INRES" \
-i :0.0 -s "$INRES" -c:v libx264 -preset "$QUAL" -b:v 3000k -maxrate 3000k -bufsize 3000k \
-vf "scale=1280:-1,format=yuv420p" -g 50 -c:a aac -b:a 256k -ar 44100 \
-f flv "$STREAM_URL"
exit 0
#-i :0.0+0,0 -c:v libx264 -preset "$QUAL" -b:v 3000k -maxrate 3000k -bufsize 3000k \

ffmpeg \
-f alsa -ac 2 -i "$INAUD" \
-f x11grab -s "$INRES" -r "$FPS" -i :0.0 \
-vcodec libx264 -b:v 3MB -s "$OUTRES" -vpre "$QUAL" \
-acodec aac  -threads 6 -qscale 5 -b:a 256k \
-f flv "$STREAM_URL"
