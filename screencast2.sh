#!/bin/bash


today=$(date +%Y%m%d-%H%M)

## Options
frameRate=25	# FPS
audioQuality=10	# Opus Qual - Bad 0 <> 10 Good

ffmpeg -loglevel 16 -threads 4 -probesize 10M \
    -video_size 1366x768  -framerate 25 -f x11grab -i :0.0+0,0 \
    -f pulse -ac 2 -i default \
    -c:v libx264rgb -crf 0 -preset ultrafast -color_range 2 \
    -c:a libopus \
    video-${today}.mkv
