#!/bin/bash

major=`git tag | sort -V | tail -n 1 | perl -e 's/\d+$//' -p `
minor=`git tag | sort -V | tail -n 1 | perl -e 's/^v(\d|\.)*\.//' -p`
new_minor=$(( $minor + 1 ))

new_version=${major}${new_minor}
git tag $new_version -a -m "New version: $new_version" && git push --tags
