#!/usr/bin/perl

my $filename = shift @ARGV;
my $numlines = shift @ARGV;
my $totlines=`wc -l $filename`;

open (FH, "<", $filename);
while (my $l=<FH>) {
    if (rand($totlines) < $numlines) {
	print $l;
	$numlines--;
    }
    $totlines--;
}
