#!/bin/bash
set -e

# Dump dei database rilevanti
for f in `find ~/.??* -name '*.sqlite'`
do
    dir=`dirname "$f"`
    base=`basename "$f"`
    cd "$dir"
    sqlite3 "$base" ".dump" > "$base".backup
done

# Delete old downloaded files and emacs backups
find ~/Downloads -mtime +10 -delete
find ~/Articoli -name '*~' -mtime +30 -delete
find ~/Devel -name '*~' -mtime +30 -delete
find ~/Articoli -name '_region_*' -mtime +10 -delete

# Sync only on cabled or wifi network
test -s /sys/class/net/eth0/address || \
    test -s /sys/class/net/eth1/address || \
    test -s /sys/class/net/wlan0/address || \
    test -s /sys/class/net/wlan1/address || \
    exit 0

# Seguono gli script che voglio eseguire una volta al giorno
sudo apt-get -qq update && sudo apt-get -qq upgrade && sudo apt-get -qq clean
# Update GPG keys
gpg --refresh-keys



# Update repositories
cd ~/.rbenv && git pull --all
cd ~/.bash_it && git pull --all
for d in ~/.rbenv/plugins/* ~/.tmux/plugins/*
do
    cd "$d" && git pull --all
done

if [ `hostname` == "vc60" ]
then

    # Delete old mails from mailing lists
    for m in `notmuch search --output=files folder:ML date:..1M`
    do
	rm -f "$m"
    done

    # Aggiorna plugin Vim
    vim +PluginInstall! +qal

    # Sposta le foto del cellulare
    test -d ~/nobackup/FotoCellulare && mv ~/nobackup/FotoCellulare/* ~/Foto/Cellulare/
    # Backup Foto
    rsync -a ~/Foto/20* pi@rp:/media/hd1/Foto/
fi
