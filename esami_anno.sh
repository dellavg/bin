#!/bin/bash

anno=$1
dir=~/Documenti/Didattica
test -d "$dir" || exit 1

cd "$dir"
for corso in *
do
    for mese in 01 02 04 06 07 09
    do
        mkdir -p "$corso"/Esami/"$anno"-"$mese"/consegne
        mkdir -p "$corso"/Esami/"$anno"-"$mese"/correzioni
    done
done
