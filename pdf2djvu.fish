#!/usr/bin/env fish

    for p in $argv
        echo "Converting $p"
        set orig (dirname "$p")
        set docp (basename "$p")
        set d (mktemp -d)
        echo "Dirs $orig -> $d"
        pdfseparate $p $d/doc-%04d.pdf
        for x in $d/doc-*.pdf
            echo "$x"
            convert "$x" "$x".pbm
            rm -f "$x"
            cjb2 -clean -dpi 600 "$x".pbm "$x".djvu
            rm -f "$x".pbm
        end
        echo "Finalizing: djvm -c $p.djvu $d/doc-*.djvu"
        djvm -c $p.djvu $d/doc-*.djvu
        #rm -fr $d
    end
