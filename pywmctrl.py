#!/usr/bin/env python3
import os
import re
import subprocess
import argparse


def clear_window(s):
    # First of all, we need to make sure to un-maximize the window
    # received as argument
    arg = "-xr " + s
    if arg == ":ACTIVE:":
        arg = "-r :ACTIVE:"
    print(f"/usr/bin/wmctrl {arg} -b remove,maximized_vert")
    subprocess.run(f"/usr/bin/wmctrl {arg} -b remove,maximized_vert", shell=True)
    subprocess.run(f"/usr/bin/wmctrl {arg} -b remove,maximized_horz", shell=True)
    subprocess.run(f"/usr/bin/wmctrl {arg} -b remove,hidden", shell=True)
    subprocess.run(f"/usr/bin/wmctrl {arg} -b remove,fullscreen", shell=True)


parser = argparse.ArgumentParser()
parser.add_argument(
    "position",
    help="where to move the window (left|center|right|left+center|right+center|none)-(top|bottom|full|none)",
    type=str,
)
# If the position is none, then we want to update the geometry of all
# windows

# Some windows need a correction factor
parser.add_argument(
    "--correction-x",
    type=int,
    default=0,
    help="correction to the x coordinate of top left vertex",
)
parser.add_argument(
    "--correction-y",
    type=int,
    default=0,
    help="correction to the y coordinate of top left vertex",
)
parser.add_argument(
    "--correction-width", type=int, default=0, help="correction to the window width"
)
parser.add_argument(
    "--correction-height", type=int, default=0, help="correction to the window heigth"
)


m = re.match("\s*([^-]+)-([^-]+)\S*$", parser.parse_args().position)
if m is None:
    help = True
else:
    (x, y) = (m.group(1), m.group(2))
    help = False

xdpyinfo = subprocess.run(["xdpyinfo"], stdout=subprocess.PIPE)
xprop_info = subprocess.run(["xprop", "-root"], stdout=subprocess.PIPE)
# Get monitor dimens@ions
wmctrl_info = subprocess.run(["wmctrl", "-d"], stdout=subprocess.PIPE)


# Look for NET_WORKAREA(CARDINAL) in the output of xprop
# It contains the coordinate of the work area
m = re.search(
    "NET_WORKAREA\(CARDINAL\) = (\d+), (\d+), (\d+), (\d+)\s*$",
    xprop_info.stdout.decode("utf-8"),
    re.MULTILINE,
)
if m is None:
    exit(1)
(left, up, screen_x, screen_y) = (
    int(m.group(1)),
    int(m.group(2)),
    int(m.group(3)),
    int(m.group(4)),
)
panel_height = up
wide_screen = screen_x > screen_y * 2

# -------------------------------------------------
# | left-top    | center-top     | right-top      |
# -------------------------------------------------
# | left-bottom | center-bottom  | right-bottom   |
# -------------------------------------------------
# |<--- 31% --->|<----- 38% ---->|<----- 31% ---->| large 16:9
# |<--- 25% --->|<----- 50% ---->|<----- 25% ---->| wide
# |<---  0% --->|<----- 100% --->|<------ 0% ---->| laptop

type = "large"
if screen_x < 3000:
    type = "wide"
if screen_x < 1920:
    type = "laptop"
print(f"Type: {type}")

# height for panels that are 32px high
portions = {
    "large": {"left": 31, "center": 38, "right": 31},
    "wide": {"left": 30, "center": 40, "right": 30},
    "laptop": {"left": 100, "center": 100, "right": 100},
}
portion = portions[type]

widths = {
    "left": (portion["left"] * screen_x) // 100,
    "center": (portion["center"] * screen_x) // 100,
    "right": (portion["right"] * screen_x) // 100,
}

height = {"top": screen_y // 2, "bottom": screen_y // 2, "full": screen_y}

horizontal_begin = {
    "left": left,
    "center": left + widths["left"],
    "right": left + widths["left"] + widths["center"],
}
widths["left+center"] = widths["left"] + widths["center"]
widths["right+center"] = widths["center"] + widths["right"]
horizontal_begin["left+center"] = horizontal_begin["left"]
horizontal_begin["right+center"] = horizontal_begin["center"]

vertical_begin = {"top": up, "bottom": up + height["top"], "full": up}

if help:
    for a in list(portion.keys()):
        for b in height.keys():
            print(
                f"{a}-{b}: {horizontal_begin[a]},{vertical_begin[b]},{widths[a]}, {height[b]}"
            )
    for a in ["left+center", "right+center"]:
        for b in ["full"]:
            print(
                f"{a}-{b}: {horizontal_begin[a]},{vertical_begin[b]},{widths[a]}, {height[b]}"
            )
    exit(0)

corrections = {
    "zulip.Zulip": {
        "large": [0, 0, 0, -30, "full", "left"],
        "wide": [0, 0, 500, -30, "full", "left"],
        "laptop": [0, 0, 0, -30, "full", "left"],
    },
    "Navigator.Zotero": {
        "large": [-20, -22, 46, 50, "full", "left"],
        "wide": [0, 0, 300, 0, "full", "left"],
        "laptop": [0, 0, 0, 0, "full", "left"],
    },
    "Newsboat.kitty": {
        "large": [0, 24, 0, -24, "full", "right+center"],
        "wide": [0, 0, 0, 0, "full", "right+center"],
        "laptop": [0, 0, 0, 0, "full", "left"],
    },
    "Neomutt.kitty": {
        "large": [0, 24, 0, -24, "full", "right+center"],
        "wide": [0, 0, 0, 0, "full", "right+center"],
        "laptop": [0, 0, 0, 0, "full", "left"],
    },
    "kitty.kitty": {
        "large": [0, 24, 0, -24, "full", "right+center"],
        "wide": [0, 0, 0, 0, "full", "right+center"],
        "laptop": [0, 0, 0, 0, "full", "left"],
    },
    "Navigator.firefox": {
        "large": [-46, -46, 92, 92, "full", "right+center"],
        "wide": [-46, -46, 92, 92, "full", "right+center"],
        "laptop": [-46, -46, 92, 92, "full", "left"],
    },
    "Google-chrome": {
        "large": [0, 0, 0, 0, "full", "right+center"],
        "wide": [0, 0, 0, 0, "full", "right+center"],
        "laptop": [0, 0, 0, 0, "full", "left"],
    },
    "code.Code": {
        "large": [0, 0, 0, 0, "full", "right+center"],
        "wide": [0, 0, 0, 0, "full", "right+center"],
        "laptop": [0, 0, 0, 0, "full", "left"],
    },
    "Brave-browser": {
        "large": [0, 0, 0, 0, "full", "right+center"],
        "wide": [-16, -10, 32, 42, "full", "right+center"],
        "laptop": [0, 0, 0, 0, "full", "left"],
    },
    "emacs.Emacs": {
        "large": [0, 0, 0, -30, "full", "right+center"],
        "wide": [0, 0, -4, -30, "full", "right+center"],
        "laptop": [0, 0, 0, -30, "full", "left"],
    },
    "org.pwmt.zathura": {
        "large": [0, 0, 0, 0, "full", "left"],
        "wide": [0, 0, 0, 0, "full", "left"],
        "laptop": [0, 0, 0, 0, "full", "left"],
    },
    "dolphin.dolphin": {
        "large": [0, 0, 0, 0, "full", "left"],
        "wide": [0, 0, 0, 0, "full", "left"],
        "laptop": [0, 0, 0, 0, "full", "left"],
    },
}

print(x, y)
if x == "none" or y == "none":
    # We need to resize all windows

    for program in corrections.keys():
        clear_window(program)
        (x, y) = (corrections[program][type][-1], corrections[program][type][-2])
        print(
            f"/usr/bin/wmctrl -xr {program} -e 0,{horizontal_begin[x] + corrections[program][type][0]},{vertical_begin[y] + corrections[program][type][1]},{widths[x] + corrections[program][type][2]},{height[y] + corrections[program][type][3]}"
        )
        subprocess.run(
            f"/usr/bin/wmctrl -xr {program} -e 0,{horizontal_begin[x] + corrections[program][type][0]},{vertical_begin[y] + corrections[program][type][1]},{widths[x] + corrections[program][type][2]},{height[y] + corrections[program][type][3]}",
            shell=True,
        )
else:
    clear_window(":ACTIVE:")
    subprocess.run(
        f"/usr/bin/wmctrl -v -r :ACTIVE: -e 0,{horizontal_begin[x]},{vertical_begin[y]},{widths[x]},{height[y]}",
        shell=True,
    )
