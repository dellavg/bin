#!/bin/bash
# License: MIT
export PATH=~/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

echo "Notmuch..."
notmuch new
#notmuch tag +sent tag:new from:gianluca@dellavedova.org
#notmuch tag +sent tag:new from:gianluca.dellavedova@unimib.it
#notmuch tag +sent tag:new from:gianluca.dellavedova@gmail.com
#notmuch tag +sent folder:Sent and not tag:sent
#notmuch tag +carla tag:new carlacorvi1@gmail.com


notmuch tag -new tag:new
