#!/bin/bash

today=$(date +%Y%m%d-%H%M)

## Options
frameRate=25    # FPS
audioQuality=10 # Opus Qual - Bad 0 <> 10 Good

audio_input=$(pactl list short sources | grep alsa_input | cut -f 1)
resolution=$(xrandr -q | grep current | head -n 1 | perl -e 's/.*current //' -p | perl -e 's/,.*//' -p |perl -e 's/ //g' -p)

ffmpeg -y -loglevel 16 -threads 4 -probesize 10M \
    -video_size $resolution  -framerate 25 -f x11grab -i :0.0+0,0 \
    -f alsa  -ac 2 -i $audio_input \
    -c:v libx264rgb -crf 0 -preset ultrafast -color_range 2 \
    -c:a libopus \
    video-${today}.mkv
